export { Utils, Point }; 

class Utils{
    static getPixelColor(ctx, point){
        function rgbToHex(r, g, b){
            if(r > 255 || g > 255 || b > 255) return "#ffffff";
            let rgb = (r << 16) | (g << 8) | (b << 0);
            return '#' + (0x1000000 + rgb).toString(16).slice(1);
        }

        let rgb = ctx.getImageData(point.x, point.y, 1, 1);
        return rgbToHex(rgb['data'][0], rgb['data'][1], rgb['data'][2]);
    }

    static findPointInCanvasFromPointInWindow(canvas, point){
        let x = point.x - canvas.getBoundingClientRect().left;
        let y = point.y - canvas.getBoundingClientRect().top;
        return new Point(x, y);
    }

    static findPointInWindowFromPointInCanvas(canvas, point){
        let x = canvas.getBoundingClientRect().left + point.x;
        let y = canvas.getBoundingClientRect().top + point.y;
        return new Point(x, y);
    }

    static findFirstResultPoint(graph, point){
        if(graph.type == 0){
            let color = Utils.getPixelColor(graph.getContext(), Utils.findPointInCanvasFromPointInWindow(graph.canvas, point));
            return [graph.getDatas().find(data => color == data.color)];
        }

        if(graph.type == 1){
            return this.#findAllPointFromX(graph, point);
        }

        if(graph.type == 2){
            let datasPossible = this.#findAllPointFromX(graph, point);
            let color = Utils.getPixelColor(graph.getContext(), Utils.findPointInCanvasFromPointInWindow(graph.canvas, point));
            for(let data of datasPossible){
                if(data.color == color) return [data];
            }
            return false;
        }
    }

    static #findAllPointFromX(graph, point){
        let datas = graph.getDatas();
        let points = graph.getPoints();
        point = Utils.findPointInCanvasFromPointInWindow(graph.canvas, point);
        let returned = new Array();
        datas = datas.sort((a, b) => { return a.length - b.length })
        let elementId = 0;

        if(Array.isArray(datas[0])){
            for(let d = 0; d < datas.length; d++){
                elementId = 0;
                for(let i = 0; i < points.length; i++){
                    if(Math.abs(points[i].x - point.x) < Math.abs(points[elementId].x - point.x)){
                        elementId = i;
                    } 
                }
                if(elementId > datas[d].length){ elementId -= datas[d].length;
                    if(Utils.isset(datas[d+1])){
                        returned.push(datas[d+1][elementId]);
                        break;
                    }
                }
                else returned.push(datas[d][elementId]);
            }
        }
        else{
            for(let i = 0; i < points.length; i++){
                if(Math.abs(points[i].x - point.x) < Math.abs(points[elementId].x - point.x)){
                    elementId = i;
                } 
            }
            returned.push(datas[elementId]);
        }

        return returned;
    }

    static findCenterOfCanvas(canvas){
        let width = canvas.width;
        let height = canvas.height;

        let x = canvas.getBoundingClientRect().left + width/2;
        let y = canvas.getBoundingClientRect().top + height/2;
        return new Point(x, y);
    }

    static isset(e){
        return e != undefined;
    }

    static textToBoolean(t){
        return t === "0" ? false : t === "1" ? true : undefined;
    }
}

class Point{
    constructor(x, y){
        this.x = x;
        this.y = y;
    }
}