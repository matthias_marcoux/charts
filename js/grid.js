import Sortable from '../node_modules/sortablejs/modular/sortable.complete.esm.js';
//https://www.npmjs.com/package/sortablejs

let nbColumns = 5;
let nbRows = 3;
let isWidgetOpen = false;
let cellSize = {};
let isSettingsOpen = false;

let gridContainer = document.getElementById('grid');
for(let y = 1; y <= nbRows; y++){
    for(let x = 1; x <= nbColumns; x++){
        let cell = document.createElement('div');
        cell.classList.add('cell');
        cell.dataset.row = y;
        cell.dataset.column = x;
        gridContainer.append(cell);
    }
}
let randomCell = gridContainer.querySelector('.cell');
cellSize.width = randomCell.offsetWidth;
cellSize.height = randomCell.offsetHeight;

gridContainer.querySelector(".icon").addEventListener('click', () => {
    document.querySelector('#widgetMenu .innerMenu').style.right = 0;
    isWidgetOpen = true;
    disableGridDragAndSort(false);
})

window.addEventListener('keyup', (event) => {
    if(isWidgetOpen){
        if(event.key == "Escape"){
            isWidgetOpen = false;
            isSettingsOpen = false;
            document.querySelector('#widgetMenu .innerMenu').style.right = "-100%";
            document.querySelector('#widgetMenu .innerSettings').style.right = "-100%";
            Array.from(document.querySelectorAll('.cell .widget.selected')).forEach((e) => e.classList.remove('selected'));
            disableGridDragAndSort(true);
        }
    }
})

let innerMenuSortable = new Sortable(document.querySelector('ul.innerMenu'), {
    group: "widget",
    drop: false,
    onStart: (e) => {
        let item = e.item;
        let clone = item.cloneNode(true);
        item.classList.add('cloned');
        item.after(clone);
        if(item.classList.contains('selected')){
            item.classList.add('selected');
            document.querySelector('#widgetMenu .innerSettings').style.right = "-100%";
        } 
    },
    onEnd: (e) => {
        if(e.target != e.to){
            if(e.item.classList.contains('cloned')) e.item.classList.remove('cloned');
            e.item.removeEventListener('click', addEventListenerDropWidget);
            e.item.addEventListener('click', addEventListenerDropWidget);

            e.item.removeEventListener('mousemove', addEventListenerGrowthWidget);
            e.item.addEventListener('mousemove', addEventListenerGrowthWidget);

            e.item.addEventListener('mouseleave', () => {disableGridDragAndSort(false)});
        }
        else e.item.remove();
    }
})

let resizable = undefined;
function addEventListenerGrowthWidget(e){
    let rect = e.target.getBoundingClientRect();
    let x = e.clientX - rect.left;
    let y = e.clientY - rect.top
    let offsetGrab = 15;

    let rightBorder = rect.right-rect.left;
    let bottomBorder = rect.bottom-rect.top;
    if(x < (rightBorder + offsetGrab) && x > rightBorder - offsetGrab){
        listenerForMouseMove(e, true);
    }
    else if(y < (bottomBorder + offsetGrab) && y > bottomBorder - offsetGrab){
        listenerForMouseMove(e, false)
    }
    else {
        e.target.style.cursor = "default";
        disableGridDragAndSort(false);
    }

    resizable = e.target;
}

function listenerForMouseMove(e, left){
    let listener = left ? moveMouseWidthEventListener : moveMouseHeightEventListener;
    let cursor = (left ? "col" : "row") + "-resize";

    e.target.style.cursor = cursor;
    disableGridDragAndSort(true);
    if(e.buttons == 1){
        e.target.removeEventListener('click', addEventListenerDropWidget);
        document.addEventListener('mousemove', listener);
        document.addEventListener('mouseup', removeMouseEventListener);
    }
}

function moveMouseWidthEventListener(e){
    let rect = resizable.getBoundingClientRect();
    let x = e.clientX - rect.left;
    resizable.style.position = "absolute";
    resizable.style.width = x+"px";
}

function moveMouseHeightEventListener(e){
    let rect = resizable.getBoundingClientRect();
    let y = e.clientY - rect.top
    resizable.style.position = "absolute";
    resizable.style.height = y+"px";
}

function removeMouseEventListener(){
    resizable.addEventListener('click', addEventListenerDropWidget);
    document.removeEventListener('mousemove', moveMouseWidthEventListener, false);  
    document.removeEventListener('mousemove', moveMouseHeightEventListener, false);      
    document.removeEventListener('mouseup', removeMouseEventListener, false);
    resizable.style.position = "relative";

    let y = resizable.offsetHeight;
    let x = resizable.offsetWidth;
    
    let rows = findNbRows(y); 
    let columns = findNbColumns(x);

    let parent = resizable.parentElement;
    let posRow = Number.parseInt(parent.dataset.row);
    let posColumn = Number.parseInt(parent.dataset.column);
    parent.style.gridArea = posRow+" / "+posColumn+" / "+(posRow+rows)+" / "+(posColumn+columns);
    console.log(parent.style.gridArea);

    resizable.style.height = "100%";
    resizable.style.width = "100%";

    recalculateColumnAndRowStart();
}

function findNbRows(height){
    for(let i = 1; i < nbRows+1; i++){
        let h = cellSize.height;
        if(height > h*i+(h/2) && height < h*(i+1)+(h/2)) return i+1;
    }
    return 1;
}

function findNbColumns(width){
    for(let i = 1; i < nbColumns+1; i++){
        let w = cellSize.width;
        if(width > w*i+(w/2) && width < w*(i+1)+(w/2)) return i+1;
    }
    return 1;
}

function recalculateColumnAndRowStart(){
    for(let cell of document.querySelectorAll('.cell')){
        let posX = cell.offsetLeft;
        let posY = cell.offsetTop;

        let globalWidth = nbColumns*cellSize.width+nbColumns*10+10;
        let globalHeight = nbRows*cellSize.height+nbRows*10+10;
        
        let row = 1;
        let col = 1;

        let calcW = globalWidth-posX;
        let restW = nbColumns;
        while(restW > 0){
            if(calcW > restW*cellSize.width){
                col = nbColumns-restW;
                break;
            } 
            else restW--;
        }

        let calcH = globalHeight-posY;
        let restH = nbRows;
        while(restH > 0){
            if(calcH > restH*cellSize.height){
                row = nbRows-restH;
                break;  
            } 
            else restH--; 
        }

        cell.dataset.row = row+1;
        cell.dataset.column = col+1;
    }
}

function addEventListenerDropWidget(){
    if(isWidgetOpen){
        let hadClass = this.classList.contains('selected');
        Array.from(document.querySelectorAll('.cell .widget.selected')).forEach((e) => e.classList.remove('selected'));
        if(!hadClass){
            this.classList.add('selected')
            document.querySelector('#widgetMenu .innerSettings').style.right = "100%";
        }
        else{
            this.classList.remove('selected')
            document.querySelector('#widgetMenu .innerSettings').style.right = "-100%";
        }
    }
}

let arrCellSortable = [];

for(let cell of document.querySelectorAll('.cell')){
    arrCellSortable.push(new Sortable(cell, {
        group: "widget",
        disabled: true,
        onAdd: (e) => {
            if(e.to.children.length > 1) e.item.remove();
        },
        onSort: (e) => {
            if(e.target != e.to){
                let arrArea = e.from.style.gridArea.split('/');
                let posRows = Number.parseInt(e.to.dataset.row);
                let posCols = Number.parseInt(e.to.dataset.column);
                e.to.style.gridArea = posRows + "/" + posCols + "/" + (posRows+(arrArea[2]-arrArea[0])) + "/" + (posCols+(arrArea[3]-arrArea[1]));
                e.from.style.gridArea = "";
                recalculateColumnAndRowStart();
            }
        }
    }));
}

function disableGridDragAndSort(bool){
    for(let sort of arrCellSortable){
        sort.options.disabled = bool;
    }
}

/*Array.from(document.querySelectorAll('.size')).forEach((e) => {
    e.addEventListener('change', () => {
        let columnInput = document.querySelector('#columnInput');
        let rowInput = document.querySelector('#rowInput');
        let selected = document.querySelector('.cell .widget.selected');
        let elem = selected.parentElement;
        let posColumn = Number.parseInt(elem.dataset.column);
        let posRow = Number.parseInt(elem.dataset.row);
        elem.style.gridArea = posRow+" / "+posColumn+" / "+(Number.parseInt(rowInput.value)+1)+" / "+(Number.parseInt(columnInput.value)+1);
    })
})*/