import Sortable from '../node_modules/sortablejs/modular/sortable.complete.esm.js';

let itemContainer = document.getElementById('containerItems');
new Sortable(itemContainer, {
    group: 'group',
    sort: false,
    onAdd: function(){
        let items = itemContainer.querySelectorAll(".item");
        items = Array.from(items).sort((a,b) => {
            return a.dataset.order >= b.dataset.order
        })
        itemContainer.innerHTML = "";
        items.forEach((e) => itemContainer.append(e))
    }
});

new Sortable(document.getElementById('container'), {
    group: 'group'
});