import { Column } from "./GraphType/Column.js";
import { Line } from "./GraphType/Line.js";
import { Pie } from "./GraphType/Pie.js";
import { Utils, Point } from "./Utils.js";

const graphs = [];
const graphsDOM = Array.from(document.querySelectorAll('.graph'));
const defaultConfigFile = "../datas/config.json";
const defaultDataFile = "../datas/datas.json";
let configFile = defaultConfigFile;
let dataFile = defaultDataFile;

for(let graph of graphsDOM){
    let datas = createDatas(null, graph);
    let config = createConfig(graph);
    createCanvas(config, graph.dataset.type, graph).draw(datas);
}

function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    let allText = false;
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                allText = rawFile.responseText;
            }
        }
    }
    rawFile.send();
    return allText;
}

(function readJSONDatas(){
    let datas = JSON.parse(readTextFile(dataFile));
    for(let graph of datas){
        createCanvas(createConfig(null, graph.config), graph.type).draw(createDatas(graph.datas));
    }
})()

function createConfig(graph = null, config = null){
    return graph ? {
        lines: Utils.textToBoolean(graph?.dataset.lines) ?? false,
        labels: Utils.textToBoolean(graph?.dataset.labels) ?? false,
        tSize: graph?.dataset.tsize ?? 14,
        ratio: graph?.dataset.ratio ?? 0.4,
        cWidth: graph?.dataset.columnwidth ?? 20,
        offsetV: graph?.dataset.offsetv ?? 15,
        offsetH: graph?.dataset.offseth ?? 10,
        title: graph?.dataset.title ?? "",
        hover: graph?.dataset.hover ?? "line",
    }
    : config ?? JSON.parse(readTextFile(configFile))
}

function createDatas(datas = null, graph = null){
    let array = [];
    if(graph){
        if(graph.querySelectorAll('.sub').length > 0){
            for(let sub of graph.querySelectorAll('.sub')){
                let subArr = [];
                for(let data of sub.querySelectorAll('data')){
                    subArr.push({
                        color: sub.dataset.globalcolor?.toLowerCase() ?? data.dataset.color?.toLowerCase() ?? "#bbbbff",
                        value: Number.parseFloat(data.value) ?? 0,
                        label: sub.dataset.globallabel ?? data.textContent,
                    })
                }
                array.push(subArr);
            }
        }
        else{
            for(let data of graph.querySelectorAll('data')){
                array.push({
                    color: data.dataset.color?.toLowerCase() ?? "#bbbbff",
                    value: Number.parseFloat(data.value) ?? 0,
                    label: data.textContent,
                })
            }
        }
    }

    if(datas){
        for(let element of datas){
            array.push(element);
        }
    }

    return array;
}

function createCanvas(config, type = null, graph = null){
    let canvas = document.createElement('canvas');
    canvas.textContent = "Votre navigateur ne supporte pas cette librairie";
    canvas.classList.add('canvas');
    canvas.setAttribute('width', graph?.dataset.w ?? 600);
    canvas.setAttribute('height', graph?.dataset.h ?? 400);
    if(!graph){
        graph = document.createElement('div');
        graph.classList.add('graph');
        document.getElementById('graph-container-js').append(graph);
        graphsDOM.push(graph);
    }
    graph.append(canvas);

    let bubble = document.createElement('div');
    bubble.classList.add('infoBox');
    graph.append(bubble)

    if(!Utils.isset(type)) type = 'Line';
    type = type.toLowerCase();
    type = type[0].toUpperCase() + type.substring(1);

    let canvasObject = getCanvasObjectFromType(type, canvas, config);
    graphs.push(canvasObject);

    return canvasObject;
}

function getCanvasObjectFromType(type, canvas, config){
    switch (type) {
        case "Pie":
            return new Pie(canvas, config);
        case "Line":
            return new Line(canvas, config);
        case "Column": 
            return new Column(canvas, config);
        default:
            return new Line(canvas, config);
    }
}

(function addMouseMoveEventListener(){
    let lastElement = undefined;
    window.addEventListener('mousemove', function(e){
        let point = new Point(e.x, e.y);
        for(let i = 0; i < graphs.length; i++){
            let bubble = graphsDOM[i].querySelector('.infoBox');
            if(graphs[i].checkIfPointIsIn(point)){
                bubble.textContent = "";
                bubble.style.display = "grid";
                bubble.style.left = e.x+"px";
                bubble.style.top = (window.scrollY+e.y+10)+"px";
    
                let elements = Utils.findFirstResultPoint(graphs[i], point);
                if(elements){
                    for(let element of elements){
                        if(Utils.isset(element)){
                            lastElement = element;
                            bubble.innerHTML += element.label == "" ? element.value : element.label + ": " +element.value+"<br>";
                        }
                        else bubble.innerHTML += lastElement.label == "" ? lastElement.value : lastElement.label + ": " +lastElement.value+"<br>";
                    }
                }

                break;
            }
            else bubble.style.display = "none";
        }
    });
})();