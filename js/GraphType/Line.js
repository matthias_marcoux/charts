import {Utils, Point} from '../Utils.js';

export {Line};

class Line{
    #ctx = undefined;
    #datas = [];
    points = [];

    constructor(canvas, config){
        this.canvas = canvas;
        this.width = canvas.width;
        this.height = canvas.height;
        this.type = config.hover == "hover" ? 2 : 1;
        this.#ctx = canvas.getContext('2d');

        this.textSize = Number.parseInt(config.tSize ?? 14);
        this.labels = config.labels ?? false;
        this.offsetV = !Utils.isset(config.offsetV) || Number.parseInt(config.offsetV)  < 15 ? 15 : Number.parseInt(config.offsetV);
        this.offsetH = !Utils.isset(config.offsetH) || Number.parseInt(config.offsetH) < 10 ? 10 : Number.parseInt(config.offsetH);
        this.title = config.title ?? "";

        this.startingPoint = this.#getPointOnCanvasFromPercentage(this.offsetH, this.offsetV);
    }

    draw(datas){
        this.#clearCanvas();
        this.#datas = datas;
        this.points = [];
        let max = 0;

        if(this.title != ""){
            this.#ctx.font = (this.textSize+10)+'px serif';
            this.#ctx.fillText(this.title, 10, this.textSize+15);
        }

        if(Array.isArray(this.#datas[0])){
            let maxElement = 0;
            this.labels = false; 
            this.#datas.forEach(e => {
                let newMax = 0;
                e.forEach(data => newMax = data.value >= newMax ? data.value : newMax);
                if(newMax > max) max = newMax;
                e.length > maxElement ? maxElement = e.length : null
            })
            this.#datas.forEach(e => this.#drawDatas(e, max, maxElement))
        }
        else{
            datas.forEach(data => max = data.value >= max ? data.value : max);
            this.#drawDatas(this.#datas, max);
        } 

        this.#drawBase(max);
    }

    #drawDatas(datas, max, nbMax = datas.length){
        let cpt = 0;
        let previousPoint = new Point(this.startingPoint.x, this.height-this.startingPoint.y);
        for(let data of datas){
            let Xpoint = this.#getPointOnLineFromPercentage(++cpt/nbMax*100, "h");
            let Ypoint = this.#getPointOnLineFromPercentage(data.value*100/max, "v");

            let point = new Point(Xpoint.x, Ypoint.y);
            this.points.push(point);

            this.#ctx.beginPath();   
                this.#ctx.strokeStyle = "black";  
                this.#ctx.globalCompositeOperation='destination-over';
                this.#ctx.arc(point.x, point.y, 6, 0, 360);
                this.#ctx.stroke();
                this.#ctx.fillStyle = data.color;
                this.#ctx.fill();
                this.#ctx.fillStyle = "black";
                if(this.labels){
                    this.#ctx.font = this.textSize+'px serif';
                    let textLenght = this.#ctx.measureText(data.label);
                    this.#ctx.fillText(data.label, point.x - textLenght.width/2, Xpoint.y);
                }
            this.#ctx.closePath();

            this.#ctx.beginPath();  
                this.#ctx.moveTo(previousPoint.x, previousPoint.y);
                this.#ctx.lineTo(point.x, point.y);
                this.#ctx.strokeStyle = "black";
                this.#ctx.stroke();
            this.#ctx.closePath();

            previousPoint = new Point(point.x, point.y);
        }
    }

    #drawBase(max){
        let ctx = this.#ctx;

        ctx.beginPath();
            ctx.strokeStyle = "black";
            ctx.moveTo(this.startingPoint.x, this.height-this.startingPoint.y);
            ctx.lineTo(this.startingPoint.x, this.startingPoint.y-15);
            ctx.moveTo(this.startingPoint.x, this.height-this.startingPoint.y);
            ctx.lineTo(this.width-this.startingPoint.x, this.height-this.startingPoint.y);
            ctx.stroke();
        ctx.closePath();

        let pers = [0, 25, 50, 75, 100];
        let valPers = pers.map(e => max*e/100);
        let dir = "v";
        this.#drawLabels(pers, valPers, dir, this.startingPoint);
    }

    #drawLabels(values, valPers, dir){
        let ctx = this.#ctx;
        let cpt = 0;
        for(let per of values){
            ctx.beginPath();    
                let point = this.#getPointOnLineFromPercentage(per, dir);
                ctx.font = '14px serif';
                let textLenght = ctx.measureText(valPers[cpt]);
                ctx.fillText(valPers[cpt++], point.x - textLenght.width - 10, point.y + this.textSize/2);
                if(dir == "v"){
                    this.#ctx.globalCompositeOperation='destination-over';
                    ctx.moveTo(point.x, point.y);
                    ctx.lineTo(this.width-this.startingPoint.x, point.y);
                    ctx.strokeStyle = "#bbbbbb";
                    ctx.stroke();
                }
            ctx.closePath();
        }
    }

    #getPointOnCanvasFromPercentage(perX, perY){
        return new Point(perX*this.width/100, perY*this.height/100);
    }

    #getPointOnLineFromPercentage(per, dir = "v"){
        if(dir == "h"){
            let distance = this.width - this.startingPoint.x*2;
            return new Point(per*distance/100 + this.startingPoint.x, this.height - this.startingPoint.y + 20);
        } 
        else{
            let distance = this.height-this.startingPoint.y*2;
            return new Point(this.startingPoint.x, distance - per*distance/100 + this.startingPoint.y);
        } 
    }

    #clearCanvas(){
        this.#ctx.clearRect(0,0,this.width, this.height);
    }

    checkIfPointIsIn(targetedPoint){
        let radius = 5;
        if(this.type == 1){
            let mousePointInCanvas = Utils.findPointInCanvasFromPointInWindow(this.canvas, targetedPoint);
            if(mousePointInCanvas.y < this.height-this.startingPoint.y && mousePointInCanvas.y > this.startingPoint.y){
                let posX = mousePointInCanvas.x < this.startingPoint.x ? this.startingPoint.x : mousePointInCanvas.x > this.width - this.startingPoint.x ? this.width - this.startingPoint.x : mousePointInCanvas.x;
                if(mousePointInCanvas.x > this.startingPoint.x && mousePointInCanvas.x < this.width - this.startingPoint.x){
                    this.draw(this.#datas);
                    this.#ctx.moveTo(posX, this.height - this.startingPoint.y);
                    this.#ctx.lineTo(posX, this.startingPoint.y);
                    this.#ctx.stroke();

                    for(let point of this.points){
                        if(mousePointInCanvas.x > point.x - radius && mousePointInCanvas.x < point.x + radius) return true;
                    }
                }
            }
        }
        else{
            radius = radius-0.5;
            for(let point of this.points){
                let center = Utils.findPointInWindowFromPointInCanvas(this.canvas, point);
                let distance = Math.sqrt(Math.pow(center.x - targetedPoint.x, 2) + Math.pow(center.y - targetedPoint.y, 2));
                if(distance < radius) return true;
            }
        }
    }

    getDatas(){
        return this.#datas;
    }

    getPoints(){
        return this.points;
    }

    getContext(){
        return this.#ctx;
    }
}