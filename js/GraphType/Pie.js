import {Utils, Point} from '../Utils.js';

export {Pie};

class Pie{
    #ctx = undefined;
    #datas = [];

    constructor(canvas, config){
        this.canvas = canvas;
        this.width = canvas.width;
        this.height = canvas.height;
        this.type = 0;
        this.#ctx = canvas.getContext('2d');

        this.textSize = Number.parseInt(config.tSize ?? 14);
        this.lines = config.lines ?? false;
        this.ratio = config.ratio ?? 0.4;
        this.title = config.title ?? "";
    }

    draw(datas){
        this.#clearCanvas();
        this.#datas = datas;

        let total = 0;
        datas.forEach(data => total += data.value);
    
        let previousAngle = 0; 

        if(this.title != ""){
            this.#ctx.font = (this.textSize+10)+'px serif';
            this.#ctx.fillText(this.title, 10, this.textSize+15);
        }

        for(let data of datas){ 
            let value = data.value;
            let color = data.color;
    
            let pourcentage = (value*100)/total;
            let coeff = (pourcentage*2)/100;
            let angle = previousAngle + Math.PI*coeff;
    
            this.#drawShapeInPie(color, previousAngle, angle);
    
            previousAngle = angle;
        }
    }

    #drawShapeInPie(color, previousAngle, angle){
        let point = this.#getPointOnPie(previousAngle);
        let newPoint = this.#getPointOnPie(angle);
        let ctx = this.#ctx;
    
        ctx.beginPath();
            ctx.fillStyle = color;
            ctx.arc(this.width/2, this.height/2, this.height > this.width ? this.width*this.ratio : this.height*this.ratio, previousAngle, angle, false);
            ctx.strokeStyle = "black";
            ctx.stroke();
            ctx.globalCompositeOperation='destination-over';
            ctx.fill();
        ctx.closePath();
        
        ctx.beginPath();
            ctx.moveTo(point.x, point.y);
            ctx.lineTo(this.width/2, this.height/2);
            ctx.lineTo(newPoint.x, newPoint.y);
            if(this.lines) ctx.stroke();
            ctx.lineTo(point.x, point.y);
            ctx.strokeStyle = color; 
            ctx.stroke();
            ctx.fill();
        ctx.closePath();
    }

    #getPointOnPie(angle){
        let radius = this.height > this.width ? this.width*this.ratio : this.height*this.ratio; 
        let center = new Point(this.width/2, this.height/2);
        return new Point(Math.round(center.x + radius * Math.cos(angle)), Math.round(center.y + radius * Math.sin(angle)));
    }

    #clearCanvas(){
        this.#ctx.clearRect(0,0,this.width, this.height);
    }

    checkIfPointIsIn(point){
        let radius = this.height > this.width ? this.width*this.ratio : this.height*this.ratio; 
        let center = Utils.findCenterOfCanvas(this.canvas);
        let distance = Math.sqrt(Math.pow(center.x - point.x, 2) + Math.pow(center.y - point.y, 2));
        return distance < radius;
    }

    getDatas(){
        return this.#datas;
    }

    getContext(){
        return this.#ctx;
    }
}